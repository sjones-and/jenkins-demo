FROM jenkins/inbound-agent
USER root
RUN apt update && apt -y upgrade
RUN apt-get -y install curl zip
USER jenkins
ENTRYPOINT ["/usr/local/bin/jenkins-agent"]
